# test-vp

## Temps passé
5h

## Difficultés
Aucune

## Améliorations possible
- integration plus poussée avec un psd et pour un travail réel
- gestion des filtres (le test devenait trop long)
- documentation du code

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


